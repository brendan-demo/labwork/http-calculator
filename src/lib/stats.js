import { linearRegression } from './regression.js';

function getRegData(statData) {
    let xValues = [];
    let yValues = [];
    let dayZero = null;
    statData.forEach(element => {
        if (!dayZero) dayZero = element[0];
        let thisDay = moment(element[0]);
        let diff = thisDay.diff(dayZero, 'days')
        xValues.push(diff);
        yValues.push(parseInt(element[1]));
    });

    const result = linearRegression(yValues, xValues);
    let lastDay = moment(dayZero).add(Math.floor(result.intercept), "days");
    let dte = lastDay.diff(moment(), 'days')
    return { result, dayZero, lastDay, dte };
}

async function calcStats(statData) {
    let regData = getRegData(statData);
    return {
        ...regData, 
        rpm: Math.round(Math.abs((regData.result.slope * 30)))
    }
}

export { calcStats, getRegData }