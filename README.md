# htTP calculator

This is a project that, backed by a Google Spreadsheet backend, will calculate how much time until *your* backend is going to need extra help.  Based on historical TPP (toilet paper performance), predict your family's TTBOPT (time-to-bidet-or-paper-towels).

## Usage

* Create a Google Spreadsheet with your daily remaining rolls count
* Publish that spreadsheet (see below for more details)
* Visit [tp.labwork.dev](https://tp.labwork.dev)
* Observe the [O'Leary Crew's data](https://twitter.com/olearycrew)
* Paste in the published CSV file URL and click "Go" to calculate your data.


## Development 
### Get started

1. Clone the repository
1. Install the dependencies with
  `npm instal`
1. Start [Rollup](https://rollupjs.org):
    ```bash
    npm run dev
    ```
1. Navigate to [localhost:5000](http://localhost:5000). 


### Building and running in production mode

To create an optimized version of the app:

```bash
npm run build
```

You can run the newly built app with `npm run start.` This uses [sirv](https://github.com/lukeed/sirv), which is included in your package.json's `dependencies` so that the app will work when you deploy to platforms like [Heroku](https://heroku.com).

### Deploying to the web
Deployed with Netlify

* Build command `npm run build`
* Deploy directory `public`

## Libraries
* Charts - [Chartist.js](https://github.com/gionkunz/chartist-js)